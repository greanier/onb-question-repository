(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): lynda
(Course Name): Designing a Presentation
(Course URL): http://www.lynda.com/Keynote-tutorials/Designing-Presentation/124082-2.html
(Discipline): professional
(ENDIGNORE)

(Type): Fill in the blank
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): One of the most important things to consider when selecting a presentation theme is selecting the right ___.
(A): Colors
(B): Image
(C): Audience
(D): Venue
(Correct): A
(Points): 1
(CF): One of the most important things to consider when selecting a presentation theme is selecting the right colors.
(WF): One of the most important things to consider when selecting a presentation theme is selecting the right colors.
(STARTIGNORE)
(Hint):
(Subject): Developing a theme
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What does the presenter consider the fun part of making presentations?
(A): Transitions
(B): Images
(C): Text
(D): Colors
(Correct): A
(Points): 1
(CF): The presenter considers selecting images to be the fun part of making presentations. 
(WF): The presenter considers selecting images to be the fun part of making presentations.
(STARTIGNORE)
(Hint):
(Subject): Developing a theme
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What are the keys to good typography?
(A): Make it fancy, big fonts, flashy.
(B): Make the lettering as big as possible, centered on the page.
(C): Keep it simple, keep them readable, have a hierarchy, and don't overdo it.
(D): Use fonts that are hard to read.
(Correct): C
(Points): 1
(CF): The keys to good typography are keeping it simple, keeping it readable, having a hierarchy, and not overdoing it.
(WF): The keys to good typography are keeping it simple, keeping it readable, having a hierarchy, and not overdoing it.
(STARTIGNORE)
(Hint):
(Subject): Developing a theme
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What is a story board?
(A): Assembly of the presentation into its own structure.
(B): A story told on a few slides.
(C): A slide with a story from a book.
(D): Slides all complete.
(Correct): A
(Points): 1
(CF): A story board is the assembly of the presentation into its own structure. It's like visualizing an attack.
(WF): A story board is the assembly of the presentation into its own structure. It's like visualizing an attack.
(STARTIGNORE)
(Hint):
(Subject): Developing a theme
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): There is no right or wrong way to do a story board.
(A): True
(B): False
(Correct): True
(Points): 1
(CF): Everyone has their own way of creating a storyboard.
(WF): Everyone has their own way of creating a storyboard.
(STARTIGNORE)
(Hint):
(Subject): Developing a theme
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): A good thing about stock photography sites is that they provide you with high quality/high res photos.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Stock photography sites do provide high quality/high res files.
(WF): Stock photography sites do provide high quality/high res files.
(STARTIGNORE)
(Hint):
(Subject): Working with images
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Typography is not used to convey a message to the audience.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Typography is used to convey a messsage to the audience. 
(WF): Typography is used to convey a messsage to the audience. 
(STARTIGNORE)
(Hint):
(Subject): Working with text
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): One of the most common goals of a presentation is to visualize data.
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): One of the most common goals of a presentation is to visualize data, whether it's sales numbers or the amount of water that's being used over time by the planet.
(WF): One of the most common goals of a presentation is to visualize data, whether it's sales numbers or the amount of water that's being used over time by the planet.
(STARTIGNORE)
(Hint):
(Subject): Showcasing data
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What are focal points?
(A): One spot on each slide.
(B): The fanciest part of the slides.
(C): The colors.
(D): How you divert the audience's attention to certain places on screen or how you control what they're looking at and what they're perceiving to b the main point.
(Correct): D
(Points): 1
(CF): How you divert the audience's attention to certain places on screen or how you control what they're looking at and what they're perceiving to b the main point.
(WF): How you divert the audience's attention to certain places on screen or how you control what they're looking at and what they're perceiving to b the main point.
(STARTIGNORE)
(Hint):
(Subject): Controlling focal points
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Too much animation does not take the focus away from the content.
(A): True
(B): False
(Correct): False
(Points): 1
(CF): Too much animation does take the focus away from the content.
(WF): Too much animation does take the focus away from the content.
(STARTIGNORE)
(Hint):
(Subject): Working with motion
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)