(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): Wikipedia
(Course Name): Software Testing
(Course URL): http://en.wikipedia.org/wiki/Software_testing
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Software testing is an investigation conducted to provide stakeholders with information about the quality of the product or service under test.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Software testing is an investigation conducted to provide stakeholders with information about the quality of the product or service under test.
(WF): Software testing is an investigation conducted to provide stakeholders with information about the quality of the product or service under test.
(STARTIGNORE)
(Hint):
(Subject): 
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Every software product has a target audience.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Every software product has a target audience. For example, the audience for video game software is completely different from banking software.
(WF): Every software product has a target audience. For example, the audience for video game software is completely different from banking software.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Software testing under all combinations of inputs and preconditions (initial state) is feasible, even with a simple product.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A fundamental problem with software testing is that testing under all combinations of inputs and preconditions (initial state) is not feasible, even with a simple product.
(WF): A fundamental problem with software testing is that testing under all combinations of inputs and preconditions (initial state) is not feasible, even with a simple product.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is true regarding software defects?
(A): All software defects are caused by coding errors.
(B): Not all software defects are caused by coding errors.
(C): Coding defects are purely the developers fault.
(D): Software defects do not exist.
(Correct): B
(Points): 1
(CF): Not all software defects are caused by coding errors. One common source of expensive defects is requirement gaps, e.g., unrecognized requirements which result in errors of omission by the program designer.
(WF): Not all software defects are caused by coding errors. One common source of expensive defects is requirement gaps, e.g., unrecognized requirements which result in errors of omission by the program designer.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In a study conducted by NIST in 2002, what was the cost to the US economy due to software bugs?
(A): $5 billion
(B): $1 billion
(C): $59.5 billion
(D): $25.4 billion
(Correct): C
(Points): 1
(CF): A study conducted by NIST in 2002 reports that software bugs cost the U.S. economy $59.5 billion annually. More than a third of this cost could be avoided if better software testing was performed.
(WF): A study conducted by NIST in 2002 reports that software bugs cost the U.S. economy $59.5 billion annually. More than a third of this cost could be avoided if better software testing was performed.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Until the 1980s, the term "software tester" was used generally, but later it was also seen as a separate profession.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Until the 1980s, the term "software tester" was used generally, but later it was also seen as a separate profession.
(WF): Until the 1980s, the term "software tester" was used generally, but later it was also seen as a separate profession.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Static testing is actually executing the code with a given set of test cases, whereas dynamic testing is the reviewing, walkthroughs/inspections.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Reviews, walkthroughs, or inspections are referred to as static testing, whereas actually executing programmed code with a given set of test cases is referred to as dynamic testing. 
(WF): Reviews, walkthroughs, or inspections are referred to as static testing, whereas actually executing programmed code with a given set of test cases is referred to as dynamic testing. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a testing method?
(A): White-Box testing
(B): Black-Box testing
(C): Visual testing
(D): Gray testing
(E): Knowledge testing
(Correct): E
(Points): 1
(CF): Knowledge testing is not a test method.
(WF): Knowledge testing is not a test method.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Alpha testing is simulated or actual operational testing by potential users/customers or an independent test team at the developers' site.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Alpha testing is simulated or actual operational testing by potential users/customers or an independent test team at the developers' site.
(WF): Alpha testing is simulated or actual operational testing by potential users/customers or an independent test team at the developers' site.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Programming groups in general stay away from automated testing.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Many programming groups are relying more and more on automated testing, especially groups that use test-driven development.
(WF): Many programming groups are relying more and more on automated testing, especially groups that use test-driven development.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)