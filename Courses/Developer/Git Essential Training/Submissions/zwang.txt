(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): www.lynda.com
(Course Name): Git Essential Training
(Course URL): http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Why is it impossible to amend a commit that is not the latest one?
(A): Causes confusion to the team members
(B): Conflicts will occur
(C): The hash key for that commit will change
(D): The hash keys for all of its ancestors will change 
(Correct): D
(Points): 1
(CF): The hash keys for all of its ancestors will change increasing Git complexity
(WF): The hash keys for all of its ancestors will change increasing Git complexity
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): It is possible but not recommended to commit a change without using 'git add' command
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): git commit -am "commit without using git add"
(WF): git commit -am "commit without using git add"
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): git pull is equivalent to which of the followings?
(A): git fetch then git merge
(B): git fetch then git add
(C): git rebase then git merge
(D): git add then git commit
(Correct): A
(Points): 1
(CF): git pull = git fetch + git merge
(WF): git pull = git fetch + git merge
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


