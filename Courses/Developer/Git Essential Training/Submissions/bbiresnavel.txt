(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brandon Bires-Navel
(Course Site): Lynda
(Course Name): Git Essential Training
(Course URL): https://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): You should always be cautious before running [code]git fetch[/code] as it can overwrite your current changes.
(A): True 
(B): False
(Correct): B
(Points): 1
(CF): [code]git fetch[/code] is a useful command that should be ran often.
(WF): [code]git fetch[/code] only gathers updates on what has changed and won't change anything in your working directory.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The HEAD refers to
(A): The most recent commit on origin/master
(B): The most recent commit on your current branch
(C): The most recent commit that has been pushed to the remote repo
(D): Each branch has a separate HEAD
(Correct): B
(Points): 1
(CF): The HEAD pointer changes anytime you switch branches.
(WF): The HEAD pointer refers to the latest commit to the branch you are currently checked into.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): By having [code]*.php[/code] in your .gitignore file [code]!index.php[/code] will not make any difference in what files are ignored.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): There are many ways to use regular expressions to ignore certain files and keep others.
(WF): There are many ways to use regular expressions to ignore certain files and keep others.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

