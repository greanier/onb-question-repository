(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brandon Bires-Navel
(Course Site): Udemy
(Course Name): Java SE 8 Developer Bootcamp
(Course URL): https://www.udemy.com/learn-java-se-8-and-prepare-for-the-java-associate-exam/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): What is the proper way to only import the static method [code]abs(int a)[/code] in the [code]java.lang.Math[/code] class?
(A): import java.lang.Math;
(B): static import java.lang.Math.*;
(C): import static java.lang.Math.*;
(D): static import java.lang.Math.abs();
(E): import static java.lang.Math.abs;
(Correct): E
(Points): 1
(CF): Static imports are useful to import static methods or fields from a class and help to reduce typing.
(WF): Static imports are useful to import static methods or fields from a class and help to reduce typing.
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a referential data type
(A): Character
(B): Integer
(C): long
(D): Object
(E): ArrayList
(Correct): C
(Points): 1
(CF): In Java there are referential data types (Integer, Character, Long, String, Object, etc.) and primitive data types (int, char, byte, long, etc.)
(WF): In Java there are referential data types (Integer, Character, Long, String, Object, etc.) and primitive data types (int, char, byte, long, etc.)
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Easy
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): When you are done using an object, you need to call the [code]finalize()[/code] method so the garbage collector knows when it can clear the memory for other objects, which speeds up your program.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Java's garbage collector does everything automatically and calling the [code]finalize()[/code] is not necessary for the programmer to do.
(WF): Java's garbage collector does everything automatically and calling the [code]finalize()[/code] is not necessary for the programmer to do.
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Of the following Objects, what objects can any of the Collections contain?
(A): Integer
(B): ArrayList
(C): byte
(D): Class
(E): boolean
(Correct): A,B,D
(Points): 1
(CF): Collections can only contain referential data types. If you want to have a list of a primitive data type you would need to use a primitive array, or use the referential equivalent of the primitive type (i.e. [code]Character[/code] instead of [code]char[/code])
(WF): Collections can only contain referential data types. If you want to have a list of a primitive data type you would need to use a primitive array, or use the referential equivalent of the primitive type (i.e. [code]Character[/code] instead of [code]char[/code])
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Which of the following are the correct way to define a method where one of the arguments is a vararg (pick three)
(A): [code]public static void main(String... args){...}[/code]
(B): [code]private void findImposter(int id, String... items, Object cloak){...}[/code]
(C): [code]void addNumbers(int a, int b, int c, int d, int e){...}[/code]
(D): [code]protected int addNumbers(int... nums){...}[/code]
(E): [code]int addNumbers(long test, long test2, int… nums){...}[/code]
(Correct): A,D,E
(Points): 1
(CF): Varargs must be the last parameter in the list of parameters.
(WF): Varargs must be the last parameter in the list of parameters.
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): What is the outcome of compiling and executing the following program?
public class Main {
    public static void main(String[] args) {
        int i = 10;
        add(i, 5);
        i++;
        System.out.println("Value = " + add(i, 7));
    }
    
    private static int add(int a, int b){
        return a * b;
    }
}
(A): 18
(B): 23
(C): 24
(D): 77
(E): 112
(Correct): D
(Points): 1
(CF): Java is pass by value and not pass by reference, thus when add(i, 5) is called the return value is ignored.
(WF): Java is pass by value and not pass by reference, thus when add(i, 5) is called the return value is ignored.
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): All classes in Java extend Object even if you don't explicitly specify that.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): All classes in Java extend and inherit from Object.
(WF): All classes in Java extend and inherit from Object.
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Which of the following are not true about Abstract classes and Interfaces (pick two)
(A): Abstract classes can only contain methods that are declared abstract
(B): Neither an abstract class nor an interface can be instantiated
(C): By default, methods in an interface are declared abstract, even if no specification is provided
(D): Abstract methods in an abstract class can be declared as private, static, and final
(Correct): A,D
(Points): 1
(CF): Abstract and Interface classes are best for forming a contract between a class and its subclasses
(WF): Abstract and Interface classes are best for forming a contract between a class and its subclasses
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
