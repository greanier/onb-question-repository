(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.

(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): Java EE Essentials: Enterprise Java Beans
(Course URL): http://www.lynda.com/Java-tutorials/Essential-Training/86005-2.html
(Discipline): Java
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): How can you handle an exception in java?
(A): You don't you just throw it all the way to the caller.
(B): Surround the code in question with a try/catch block.
(C): Use system.err.println
(D): None of the above
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 7 - Exception handling
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): ArrayList automatically resize when adding data.
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 8 - Using data collections
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): If your custom class does not have a no args constructor java will NOT provide one for you?
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 9 - Create Custom Classes
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What will the output look like for the following snippet of code: 
	byte b = 127;
	System.out.println(b);
(A): 0 
(B): "one hundred twenty seven"
(C): 127
(D): Undefined exception
(Correct): C
(Points): 1
(CF): Correct!
(WF): See Part 4 - Using primitive data types
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What will the output look like for the following snippet of code: 
	int i = 50;
	System.out.println("There are " + i + " dogs in the park");
(A): There are 50 dogs in the park
(B): invalid string operation exception
(C): null pointer exception 
(D): None of the above
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 4 - Using primitive data types
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
