(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji
(Course Site): Lynda
(Course Name): Business Analysis Fundamentals
(Course URL): http://www.lynda.com/Business-Skills-tutorials
(Discipline): Business Analysis
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is the Top layer of the requirements pyramid?
(A): Your requirement
(B): Execution
(C): Specs and Design
(D): Project Need
(Correct): D
(Points): 1
(CF): The top layer of the requirements pyramid is "Project Need".
(WF): The top layer of the requirements pyramid is "Project Need".
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Requirements define _______ is needed, not the ______.
(A): who, how
(B): what, why
(C): what, how
(D): who, when
(Correct): C
(Points): 1
(CF): Requirements define what is needed, not the how.
(WF): Requirements define what is needed, not the how.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): In a project life cycle, the Development team can also be known as _________?
(A): Input people 
(B): Output people
(C): Ending people
(D): Project managers
(E): Business Analysts
(Correct): B
(Points): 1
(CF): The Development team are also known as Output people, people who are in charge of outputting the requirements given by PO's/Stakeholders (who provide the input).
(WF): The Development team are also known as Output people, people who are in charge of outputting the requirements given by PO's/Stakeholders (who provide the input).
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): This is one of the top 3 reasons for project success...
(A): Project management
(B): User involvement
(C): Disabling features
(D): Training stakeholders
(Correct): B
(Points): 1
(CF): User involvement is one of the top 3 reasons for project success (according to the lynda.com course)
(WF): User involvement is one of the top 3 reasons for project success (according to the lynda.com course)
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When gathering requirements from Stakeholders, which open-ended question is generally asked last?
(A): The "How"
(B): The "Who"
(C): The "Where"
(D): The "When"
(Correct): A
(Points): 1
(CF): The "How" is generally asked last, as it leads to a more-specific answer.
(WF): The "How" is generally asked last, as it leads to a more-specific answer.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): The KRAC approach stands for "Keep, Remove, Add, Clear".
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): The KRAC approach stands for "Keep, Remove, Add, CHANGE".
(WF): The KRAC approach stands for "Keep, Remove, Add, CHANGE".
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): Analysis Paralysis results in delays and frustrations, as business improvements are not realized in appropriate time-frames.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): True. It's best to avoid Analysis Paralysis is to know when one has completed collecting requirements.
(WF): True. It's best to avoid Analysis Paralysis is to know when one has completed collecting requirements.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): If 9 of 10 people have a common understanding in your requirement, is it valid?
(A): Yes, more likely
(B): No, need more input so 10 of 10 can understand it
(C): Not sure
(Correct): A
(Points): 1
(CF): If 9 of 10 people have a common understanding in your requirement, it is valid.
(WF): If 9 of 10 people have a common understanding in your requirement, it is valid.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): A Business Analyst is also known as a ship's ____________.
(A): Navigator
(B): Captain
(C): Lieutenant
(D): Sargent
(E): King
(Correct): A
(Points): 1
(CF): A Business Analyst is also known as a ship's Navigator.
(WF): A Business Analyst is also known as a ship's Navigator.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which is NOT a part of the Initiation Phase?
(A): Manage business objectives
(B): Ensure objectives are valid
(C): Manage collection of requirements
(Correct): C
(Points): 1
(CF): Manage collection of requirements is part of the PLANNING phase.
(WF): Manage collection of requirements is part of the PLANNING phase.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
