(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essentials Training

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph DiBrango
(Course Site): lynda
(Course Name): SQL Essentials Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What does DBMS stand for?
(A): Database Micro System
(B): Database Management System
(C): Database Microsoft System
(D): None of the above
(Correct): B
(Points): 1
(CF): DBMS is a Database Management System.
(WF): DBMS is a Database Management System.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 1
(Random answers): 1
(Question): Which items are part of the CRUD acronym (select all that apply)?
(A): CREATE
(B): UPDATE
(C): DELETE
(D): READ
(E): RETRIEVE
(Correct): A,B,C,D,E
(Points): 1
(CF): Create, Read (retrieve), Update, Delete (CRUD)
(WF): Create, Read (retrieve), Update, Delete (CRUD)
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Double dash -- represents what in an sql statement?  
(A): Comment
(B): Minus Minus
(C): OR
(D): AND
(Correct): A
(Points): 1
(CF): A double dash is a comment style.
(WF): A double dash is a comment style.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): In the following statement: CREATE TABLE test(a INTEGER, b TEXT); What is the part in between the parenthesis called?
(A): Table
(B): Schema
(C): Row
(D): Column
(Correct): B 
(Points): 1
(CF): It is called the schema, the structure of a database system.
(WF): It is called the schema, the structure of a database system.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 1
(Random answers): 1
(Question): What SQL keywords or operators are used to check for NULL values (select all that apply)?
(A): ==
(B): IS NULL 
(C): =
(D): IS NOT NULL
(Correct): B,D
(Points): 1
(CF): NULL is a special state in SQL, it's not really a value, it's the lack of value.  Use IS NULL or IS NOT NULL to check for this value.
(WF): NULL is a special state in SQL, it's not really a value, it's the lack of value.  Use IS NULL or IS NOT NULL to check for this value.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 1
(Random answers): 1
(Question): What keywords below are considered constraints (select all that apply)?
(A): NOT NULL
(B): UNIQUE
(C): DEFAULT
(D): SELECT
(Correct): A,B,C
(Points): 1
(CF): DEFAULT, UNIQUE and NOT NULL are considered constraints.
(WF): DEFAULT, UNIQUE and NOT NULL are considered constraints.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What keyword is used to read data from a database table? 
(A): CREATE
(B): UPDATE
(C): INSERT
(D): SELECT
(Correct): D
(Points): 1
(CF): The Select statements reads or retrieves data from a database table.
(WF): The Select statements reads or retrieves data from a database table.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Unique id columns are automatically populated using what keyword?
(A): UPDATE
(B): FOREIGN KEY
(C): PRIMARY KEY
(D): None of the above
(Correct): C
(Points): 1
(CF): (PRIMARY KEY) ID columns are columns that hold a unique value for each row in a table. Typically, ID columns are automatically populated.
(WF): (PRIMARY KEY) ID columns are columns that hold a unique value for each row in a table. Typically, ID columns are automatically populated.
(STARTIGNORE)
(Hint):
(Subject): Relationships
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): The DISTINCT keyword performs the following function?
(A): Sorts results
(B): Reads data from a table
(C): Inserts data into a table
(D): Removes duplicate results
(Correct): D
(Points): 1
(CF): DISTINCT - removes duplicates from the result.
(WF): DISTINCT - removes duplicates from the result.
(STARTIGNORE)
(Hint):
(Subject): Strings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Triggers are operations that are automatically performed when a specified database event occurs.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Triggers happen when a specified database event occurs.
(WF): Triggers happen when a specified database event occurs.
(STARTIGNORE)
(Hint):
(Subject): Numbers
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)