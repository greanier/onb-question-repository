(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essentials Training

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): lynda.com
(Course Name): SQL Essentials Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Which of the below is the equivalent of a WHERE clause, but for aggregate data?
(A): WHEN
(B): HAVING
(C): GROUP BY
(D): LIKE
(E): IN
(Correct): B
(Points): 1
(CF): You should use HAVING when you want to work with aggregate data created by a GROUP BY.
(WF): You should use HAVING when you want to work with aggregate data created by a GROUP BY.
(STARTIGNORE)
(Hint):
(Subject): SQL - general
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): SQL is considered a relational database.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): SQL is a relational database.
(WF): SQL is a relational database.
(STARTIGNORE)
(Hint):
(Subject): SQL - general
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): SQL string manipulation, date syntaxes, and joins are standardized across all SQL software.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Although many versions may be the same or similar, you should be careful to check the documentation for the product you are using.
(WF): Although many versions may be the same or similar, you should be careful to check the documentation for the product you are using.
(STARTIGNORE)
(Hint):
(Subject): SQL - general
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What is a SQL transaction?
(A): Creation of an adjunction table to manage the relationships between other tables.
(B): Renaming a table in the database.
(C): A group of operations done as one unit of work.
(D): None of the above.
(Correct): C
(Points): 1
(CF): Transactions can be useful for reducing overhead or canceling all the operations if any single one fails.
(WF): Transactions can be useful for reducing overhead or canceling all the operations if any single one fails.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What is VIEW used for?
(A): Reordering the columns of a table.
(B): Reordering the rows of a table.
(C): Presenting the output result in an alternative format.
(D): Saving a query that can then be used as an input in other queries.
(Correct): D
(Points): 1
(CF): A VIEW is essentially a saved query.
(WF): A VIEW is essentially a saved query.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What are triggers useful for?
(A): Automatically adding data to newly created tables.
(B): Running many queries in rapid succession.
(C): Automatically running a command after some of the rows are modified on a specified table.
(D): Ensuring a primary key remains valid.
(Correct): C
(Points): 1
(CF): Triggers are attached to a table and run after an INSERT, UPDATE, or DELETE is performed on the table.
(WF): Triggers are attached to a table and run after an INSERT, UPDATE, or DELETE is performed on the table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)