(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Fred Thomas
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Agile project management differs from traditional waterfall methodologies by
(A): The business can receive more frequent deliveries of pieces of the overall functionality. 
(B): Requirements are better documented up-front.
(C): Changes to features are more quickly accepted and are actually expected
(D): Development time is expedited because it is broken into smaller pieces.
(E): It reduces inter-system business dependencies.
(Correct): A,C
(Points): 1
(CF): With agile projects, items are created by a small logical chunks of work called iterations or sprints. Agile is a great technique to use when business needs are frequently changing or when the business wants to receive product benefits earlier. With Agile you can focus on what the business needs now and if that changes the new business needs can be accommodated in the next iteration.
(WF): With agile projects, items are created by a small logical chunks of work called iterations or sprints. Agile is a great technique to use when business needs are frequently changing or when the business wants to receive product benefits earlier. With Agile you can focus on what the business needs now and if that changes the new business needs can be accommodated in the next iteration.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Which stages of the agile project management lifecycle are repeated in each sprint?
(A): Speculate
(B): Envision
(C): Explore
(D): Adapt
(E): Close
(Correct): A,C,D
(Points): 1
(CF): The Speculate, Explore and Adapt stages are repeated with each sprint.
(WF): The Envision and Close stages happen only once and the beginning and end of a project, which may contain multiple sprints.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): At the end of the Envision stage, the team should have the following:
(A): Project scope definition
(B): Tools that will be used
(C): Initial estimates on features
(D): Complete documentation for each feature
(E): A finished working product
(Correct): A,B
(Points): 1
(CF): After the Envision stage you should have a project charter which includes objectives, scope and stakeholders.  Tools to be used by the team should be in place as well as defined team norms.
(WF): Feature estimates are done during each sprint.  Initial estimates for all features will be done as part of the first sprint.  Complete feature documentation does not need to be ready at the very beginning of the project, but will be needed on a specific feature before the team can consider including it in a sprint. 
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): It is recommended to keep agile teams limited to:
(A): 5 or less
(B): 10 or less
(C): 15 or less
(D): 25 or less
(E): It doesn't really matter
(Correct): C
(Points): 1
(CF): It is recommended to keep agile teams to 15 members or less.  Larger teams introduce risk, as the coordination between larger numbers of people and the features they produce is much more difficult.
(WF): It is recommended to keep agile teams to 15 members or less.  Larger teams introduce risk, as the coordination between larger numbers of people and the features they produce is much more difficult.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Sprint Structure refers to:
(A): The Speculate, Explore and Adapt stages repeated in each sprint
(B): The number of team members and their roles
(C): The specific features that will be built during an individual sprint
(D): The duration and the number of features you'll try to build for each sprint
(E): The physical location of team members which is especially important when the entire team is not co-located.
(Correct): D
(Points): 1
(CF): The sprint structure is the length of your sprints and the number of features you'll try to build during each sprint.
(WF): The sprint structure is the length of your sprints and the number of features you'll try to build during each sprint.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The ScrumMaster can best be described as
(A): The team leader who assigns features to each member of the team based on their ability.
(B): The person who drives the daily stand-up meeting to make sure it is kept on-time.
(C): The guy who cleans the hull of your boat at the marina.
(D): The business analyst who is ensuring the features for the next spring have been clearly defined.
(E): The project manager responsible for ensuring the team is flowing productively
(Correct): E
(Points): 1
(CF): The project manager is often called the ScrumMaster. 
(WF): The project manager is often called the ScrumMaster. 
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Ideally each team member should speak at the daily stand-up meeting ...
(A): only if they have an issue or question for another team member
(B): for 30 to 60 seconds
(C): for 2 or 3 minutes
(D): as long as needed to communicate their design plans for the feature they will be working on that day
(E): all at the same time
(Correct): D
(Points): 1
(CF): Ideally, each member of a conditioned team should be able to give a brief update of yesterday's accomplishments and plans for today in a 30 to 60 second window.
(WF): Ideally, each member of a conditioned team should be able to give a brief update of yesterday's accomplishments and plans for today in a 30 to 60 second window.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The PDCA Cycle is
(A): Another name for stages that occur during each sprint
(B): A technique to foster collaborative behavior.
(C): A process that can be used during the Speculate stage to come to collective agreement on feature estimates.
(D): A process for determining which features will be included in a sprint.
(E): The way business teams often refer to the Agile software development life cycle.
(Correct): B
(Points): 1
(CF): The PDCA Cycle (plan, do, check, adjust) is a great technique to foster collaborative behavior between team members.
(WF): The PDCA Cycle (plan, do, check, adjust) is a great technique to foster collaborative behavior between team members.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who determines the priority and order in which features will be implemented?
(A): Business
(B): ScrumMaster
(C): Technical Team
(D): Project Manager
(E): Business with input from the technical team where it may affect effort.
(Correct): E
(Points): 1
(CF): The business sets the priority for the features, but it is the responsibility of the team to advise them if there are efficiencies that can be gained by adjusting the prioritization.  Ultimately, the business still has the final decision.
(WF): The business sets the priority for the features, but it is the responsibility of the team to advise them if there are efficiencies that can be gained by adjusting the prioritization.  Ultimately, the business still has the final decision.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The close and adapt stages are great for addressing issues and making changes.  During the sprint, ideas or areas for improvement should be
(A): jotted down with enough detail to have context
(B): shared with the team so everyone can consider them
(C): jotted down along with suggestions, otherwise it's just whining
(D): kept in mind until it is time to share them at the meeting
(E): overlooked because everyone and every process has flaws - why rock the boat?
(Correct): A
(Points): 1
(CF): Team members should jot down lessons learned at any time. Insure the information is complete, so you have the context. Keep in mind, you don't need to know how you're going to resolve the issue. Just get the feedback written down.
(WF): Team members should jot down lessons learned at any time. Insure the information is complete, so you have the context. Keep in mind, you don't need to know how you're going to resolve the issue. Just get the feedback written down.
(STARTIGNORE)
(Hint):
(Subject): Life Cycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
