
(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jacob Lista
(Course Site): lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How often does the Envision phase of an agile project occur?
(A): Once per sprint
(B): Once per project
(C): Once per week
(D): Multiple times per sprint as needed
(Correct): A
(Points): 1
(CF): The Envision phase occurs at the beginning of a sprint
(WF): Agile is an iterative process, but only certain phases are repeated as needed.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following should be included on a Project Data Sheet?
(A): Description of the project
(B): Constraints
(C): Cost Estimate
(D): Positions of the development team members
(E): Order of tasks on the backlog
(Correct): A,B,C
(Points): 1
(CF): The PDS is created up front. It contains information about the project itself, not specifically how the Agile team operates.
(WF): The PDS is created up front. It contains information about the project itself, not specifically how the Agile team operates.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is helpful to make use case diagrams during which phase of the agile process?
(A): Envision
(B): Speculate
(C): Explore
(D): Adapt
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

